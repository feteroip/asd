public static void InsertionSort(LinkedList<int> list)
{
    var currentNode = list.First;

    while (currentNode != null)
    {
        var nextNode = currentNode.Next;

        while (nextNode != null && nextNode.Value < currentNode.Value)
        {
            var temp = nextNode.Value;
            list.Remove(nextNode);
            var beforeCurrent = currentNode.Previous;

            if (beforeCurrent == null)
            {
                list.AddFirst(nextNode);
            }
            else
            {
                list.AddAfter(beforeCurrent, nextNode);
            }

            nextNode = currentNode.Next;
        }

        currentNode = currentNode.Next;
    }
}