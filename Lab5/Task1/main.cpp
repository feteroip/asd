public static void SelectionSort(LinkedList<int> list)
{
    var currentNode = list.First;

    while (currentNode != null)
    {
        var minNode = currentNode;
        var nextNode = currentNode.Next;

        while (nextNode != null)
        {
            if (nextNode.Value < minNode.Value)
            {
                minNode = nextNode;
            }
            nextNode = nextNode.Next;
        }

        list.Remove(minNode);
        list.AddBefore(currentNode, minNode);
        currentNode = currentNode.Next;
    }
}