#include <iostream>
#include <chrono>
#include <iomanip>

void bubbleSort(float arr[], int n) {
    for (int i = 0; i < n - 1; ++i) {
        for (int j = 0; j < n - i - 1; ++j) {
            if (arr[j] < arr[j + 1]) {
                float temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

int main() {
    int m;
    std::cout << "Введіть розмір масиву: ";
    std::cin >> m;

    if (m <= 0 || m > 1000) {
        std::cerr << "Помилка: неправильний розмір масиву." << std::endl;
        return 1;
    }

    float arr[m];
    for (int i = 0; i < m; ++i) {
        arr[i] = static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * 1000;
    }

    auto start = std::chrono::high_resolution_clock::now();
    bubbleSort(arr, m);
    auto end = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double> duration = end - start;
    std::cout << "Час сортування: " << std::setprecision(9) << std::fixed << duration.count() << " секунд" << std::endl;

    std::cout << "Масив успішно відсортований." << std::endl;

    return 0;
}