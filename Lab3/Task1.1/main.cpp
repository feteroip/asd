#include <iostream>
#include <chrono>
#include <iomanip>

double power(int a, int b) {
    double result = 1.0;
    for (int i = 0; i < b; i++) {
        result *= a;
    }
    return result;
}

int main() {
    int a, b;
    std::cout << "Enter (a): ";
    std::cin >> a;
    std::cout << "Enter (b): ";
    std::cin >> b;

    auto start = std::chrono::high_resolution_clock::now();
    double result = power(a, b);
    auto end = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double> time_taken = end - start;
    std::cout << "Computation time: " << std::fixed << std::setprecision(9) << time_taken.count() << " seconds" << std::endl;

    std::cout << a << " raised to the power of " << b << " is " << std::fixed << std::setprecision(9) << result << std::endl;

    return 0;
}