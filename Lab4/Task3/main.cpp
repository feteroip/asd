#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

#define MAX_SIZE 100

typedef struct {
    double data[MAX_SIZE];
    int top;
} Stack;

void initialize(Stack *s) {
    s->top = -1;
}

void push(Stack *s, double value) {
    if (s->top == MAX_SIZE - 1) {
        printf("Stack overflow\n");
        exit(EXIT_FAILURE);
    }
    s->data[++s->top] = value;
}

double pop(Stack *s) {
    if (s->top == -1) {
        printf("Stack underflow\n");
        exit(EXIT_FAILURE);
    }
    return s->data[s->top--];
}

double evaluate_expression(char *expression) {
    Stack s;
    initialize(&s);
    char *token = strtok(expression, " ");
    while (token != NULL) {
        if (isdigit(*token) || (*token == '-' && isdigit(*(token + 1)))) {
            push(&s, atof(token));
        } else {
            double op2 = pop(&s);
            double op1 = pop(&s);
            switch (*token) {
                case '+':
                    push(&s, op1 + op2);
                    break;
                case '-':
                    push(&s, op1 - op2);
                    break;
                case '*':
                    push(&s, op1 * op2);
                    break;
                case '/':
                    push(&s, op1 / op2);
                    break;
                case '^':
                    push(&s, pow(op1, op2));
                    break;
                case 's':
                    if (*(token + 1) == 'q' && *(token + 2) == 'r' && *(token + 3) == 't') {
                        push(&s, sqrt(op2));
                    } else {
                        printf("Invalid operation\n");
                        exit(EXIT_FAILURE);
                    }
                    break;
                default:
                    printf("Invalid operation\n");
                    exit(EXIT_FAILURE);
            }
        }
        token = strtok(NULL, " ");
    }
    return pop(&s);
}

int main() {
    char expression[MAX_SIZE];
    printf("Enter the expression in reverse Polish notation: ");
    fgets(expression, MAX_SIZE, stdin);
    double result = evaluate_expression(expression);
    printf("Result: %.2f\n", result);
    return 0;
}