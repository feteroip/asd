#include <stdio.h>
#include <stdlib.h>

typedef int elemtype;

struct elem {
    elemtype value;
    struct elem* next;
    struct elem* prev;
};

struct myList {
    struct elem* head;
    struct elem* tail;
    size_t size;
};

typedef struct elem cNode;
typedef struct myList cList;

cList* createList() {
    cList* list = (cList*)malloc(sizeof(cList));
    if (list) {
        list->size = 0;
        list->head = list->tail = NULL;
    }
    return list;
}


void deleteList(cList* list) {
    cNode* head = list->head;
    cNode* next = NULL;
    while (head) {
        next = head->next;
        free(head);
        head = next;
    }
    free(list);
    list = NULL;
}

bool isEmptyList(cList* list) {
    return ((list->head == NULL) || (list->tail == NULL));
}

int pushFront(cList* list, elemtype* data) {
    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node) {
        return -1;
    }
    node->value = *data;
    node->next = list->head;
    node->prev = NULL;
    if (!isEmptyList(list)) {
        list->head->prev = node;
    } else {
        list->tail = node;
    }
    list->head = node;
    list->size++;
    return 0;
}

int popFront(cList* list, elemtype* data) {
    cNode* node;
    if (isEmptyList(list)) {
        return -2;
    }
    node = list->head;
    list->head = list->head->next;
    if (!isEmptyList(list)) {
        list->head->prev = NULL;
    } else {
        list->tail = NULL;
    }
    *data = node->value;
    list->size--;
    free(node);
    return 0;
}

int pushBack(cList* list, elemtype* data) {
    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node) {
        return -3;
    }
    node->value = *data;
    node->next = NULL;
    node->prev = list->tail;
    if (!isEmptyList(list)) {
        list->tail->next = node;
    } else {
        list->head = node;
    }
    list->tail = node;
    list->size++;
    return 0;
}

int popBack(cList* list, elemtype* data) {
    cNode* node = NULL;
    if (isEmptyList(list)) {
        return -4;
    }
    node = list->tail;
    list->tail = list->tail->prev;
    if (!isEmptyList(list)) {
        list->tail->next = NULL;
    } else {
        list->head = NULL;
    }
    *data = node->value;
    list->size--;
    free(node);
    return 0;
}

cNode* getNode(cList* list, int index) {
    cNode* node = NULL;
    int i;
    if (index >= list->size) {
        return NULL;
    }
    if (index < list->size / 2) {
        i = 0;
        node = list->head;
        while (node && i < index) {
            node = node->next;
            i++;
        }
    } else {
        i = list->size - 1;
        node = list->tail;
        while (node && i > index) {
            node = node->prev;
            i--;
        }
    }
    return node;
}

int insert(cList* list, int index, elemtype* value) {
    cNode* elm = NULL;
    cNode* ins = NULL;
    elm = getNode(list, index);
    if (elm == NULL) {
        return -5;
    }
    ins = (cNode*)malloc(sizeof(cNode));
    ins->value = *value;
    ins->prev = elm;
    ins->next = elm->next;
    if (elm->next) {
        elm->next->prev = ins;
    }
    elm->next = ins;
    if (!elm->prev) {
        list->head = elm;
    }
    if (!elm->next) {
        list->tail = elm;
    }
    list->size++;
    return 0;
}

int deleteNode(cList* list, int index, elemtype* data) {
    cNode* elm = NULL;
    elemtype tmp = 0;
    elm = getNode(list, index);
    if (elm == NULL) {
        return -6;
    }
    if (elm->prev) {
        elm->prev->next = elm->next;
    }
    if (elm->next) {
        elm->next->prev = elm->prev;
    }
    tmp = elm->value;
    if (!elm->prev) {
        list->head = elm->next;
    }
    if (!elm->next) {
        list->tail = elm->prev;
    }
    *data = elm->value;
    free(elm);
    list->size--;
    return 0;
}

void printNode(elemtype* value) {
    printf("%d\n", *value);
}

void printList(cList* list, void (*func)(elemtype*)) {
    cNode* node = list->head;
    if (isEmptyList(list)) {
        return;
    }
    while (node) {
        func(&node->value);
        node = node->next;
    }
}

int main() {
    cList* mylist = createList();
    elemtype tmp;
    int task = 0;
    int index;
    do {
        printf("What would you like to do?\n");
        printf("1) Add an element to the beginning of the list\n");
        printf("2) Add an element to the end of the list\n");
        printf("3) Add an element after a specified index\n");
        printf("4) Delete the first element of the list\n");
        printf("5) Delete the last element of the list\n");
        printf("6) Delete an element at a specified index\n");
        printf("7) Enter a specific element\n");
        printf("8) Print the entire list\n");
        printf("9) Delete the list\n");
        printf("0) Exit\n");
        scanf("%d", &task);
        elemtype elem;
        switch (task) {
            case 1:
                printf("\n");
                printf("Enter the value of the element\n");
                scanf("%d", &elem);
                pushFront(mylist, &elem);
                printf("\n");
                break;
            case 2:
                printf("\n");
                printf("Enter the value of the element\n");
                scanf("%d", &elem);
                pushBack(mylist, &elem);
                printf("\n");
                break;
            case 3:
                printf("\n");
                printf("Enter the value of the element\n");
                scanf("%d", &elem);
                printf("Enter the index after which to insert the element\n");
                scanf("%d", &index);
                insert(mylist, index, &elem);
                printf("\n");
                break;
            case 4:
                printf("\n");
                popFront(mylist, &tmp);
                printf("Element deleted\n");
                printf("\n");
                break;
            case 5:
                printf("\n");
                popBack(mylist, &tmp);
                printf("Element deleted\n");
                printf("\n");
                break;
            case 6:
                printf("\n");
                printf("Enter the index to delete\n");
                scanf("%d", &index);
                deleteNode(mylist, index, &tmp);
                printf("\n");
                break;
            case 7:
                printf("\n");
                printf("Enter the index to print\n");
                scanf("%d", &index);
                printNode(&getNode(mylist, index)->value);
                printf("\n");
                break;
            case 8:
                printf("\n");
                printList(mylist, printNode);
                printf("\n");
                break;
            case 9:
                deleteList(mylist);
                break;
        }
        getchar();
        printf("Press Enter to continue...");
        getchar();
//        system("clear"); //для лінукса
        system("cls"); // для Windows
    } while (task != 0);
    return 0;
}