using System;
using System.Diagnostics;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Виберіть метод сортування:");
        Console.WriteLine("1. Пірамідальне сортування");
        Console.WriteLine("2. Сортування підрахунком");

        int choice = int.Parse(Console.ReadLine());

        int[] dataSizes = { 10, 100, 500, 1000, 2000, 5000, 10000 };

        foreach (int dataSize in dataSizes)
        {
            Console.WriteLine($"Розмір даних: {dataSize}");

            int[] data = GenerateRandomData(dataSize);
            Stopwatch stopwatch = new Stopwatch();


            switch (choice)
            {
                case 1:
                    stopwatch.Start();
                    HeapSort(data);
                    stopwatch.Stop();
                    break;
                case 2:
                    stopwatch.Start();
                    CountingSort(data);
                    stopwatch.Stop();
                    break;
                default:
                    Console.WriteLine("Неправильний вибір методу сортування.");
                    return;
            }

            Console.WriteLine($"Час сортування: {stopwatch.Elapsed.TotalMilliseconds} мс");
        }
    }

    static int[] GenerateRandomData(int size)
    {
        Random random = new Random();
        int[] data = new int[size];

        for (int i = 0; i < size; i++)
        {
            data[i] = random.Next(-10, 101); // для пірамідального
            //data[i] = random.Next(-35, 16); // для сортування підрахунком
        }

        return data;
    }

    static void HeapSort(int[] data)
    {
        int n = data.Length;

        for (int i = n / 2 - 1; i >= 0; i--)
            Heapify(data, n, i);

        for (int i = n - 1; i >= 0; i--)
        {
            int temp = data[0];
            data[0] = data[i];
            data[i] = temp;

            Heapify(data, i, 0);
        }
    }

    static void Heapify(int[] data, int n, int i)
    {
        int largest = i;
        int left = 2 * i + 1;
        int right = 2 * i + 2;

        if (left < n && data[left] > data[largest])
            largest = left;

        if (right < n && data[right] > data[largest])
            largest = right;

        if (largest != i)
        {
            int swap = data[i];
            data[i] = data[largest];
            data[largest] = swap;

            Heapify(data, n, largest);
        }
    }

    static void CountingSort(int[] data)
    {
        int min = data.Min();
        int max = data.Max();
        int range = max - min + 1;
        int[] count = new int[range];
        int[] output = new int[data.Length];

        for (int i = 0; i < data.Length; i++)
            count[data[i] - min]++;

        for (int i = 1; i < count.Length; i++)
            count[i] += count[i - 1];

        for (int i = data.Length - 1; i >= 0; i--)
        {
            output[count[data[i] - min] - 1] = data[i];
            count[data[i] - min]--;
        }

        for (int i = 0; i < data.Length; i++)
            data[i] = output[i];
    }
}